/* See LICENSE file for copyright and license details. */

/* necessary to use media keys */
#include <X11/XF86keysym.h>

/* theme */
#include "themes/base16-ocean.h"

/* appearance */
static const int CORNER_RADIUS           = 0;
static const unsigned int borderpx       = 3;    /* border pixel of windows */
static const unsigned int gappx          = 18;   /* gap pixel between windows */
static const unsigned int snap           = 15;   /* snap pixel */
static const unsigned int systraypinning = 0;    /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 9;    /* systray spacing */
static const int systraypinningfailfirst = 1;    /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 0;    /* 0 means no systray */
static const int showbar                 = 1;    /* 0 means no bar */
static const int topbar                  = 1;    /* 0 means bottom bar */
static const int horizpadbar             = 0;    /* horizontal padding for statusbar */
static const int vertpadbar              = 14;   /* vertical padding for statusbar */
static const char barheight[]            = "25"; /* dmenu line height */
static const char *fonts[]               = { "curie:size=12:style=Regular", "waffle", "FuraCode Nerd Font:size=9:antialias=true:autohint=true:width=1" };
static const char dmenufont[]            = "curie:size=12";

/* tagging */
static const char *tags[] = { "Web", "Dev", "Fun", "Chat", "Down"  };

/* rules */
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class               instance    title       tags mask     isfloating   monitor */
	{ "feh",               NULL,       NULL,            0,  		1,           -1 },
	{ "Firefox",           NULL,       NULL,       1 << 0,  		0,           -1 },
	{ "Gimp",              NULL,       NULL,       1 << 2,          0,           -1 },
	{ "Steam",             NULL,       NULL,       1 << 2,  		0,           -1 },
	{ "Spotify",           "spotify",  NULL,       1 << 2,  		0,           -1 },
	{ "retroarch",         NULL,       NULL,       1 << 2,  		0,           -1 },
	{ "discord",           NULL,       NULL,       1 << 3,  		0,           -1 },
	{ "TelegramDesktop",   NULL,       NULL,       1 << 3,  		0,           -1 },
	{ "Slack",             NULL,       NULL,       1 << 3,  		0,           -1 },
	{ "qBittorrent",       NULL,       NULL,       1 << 4,          0,           -1 },
	{ "soulseekqt",        NULL,       NULL,       1 << 4,          0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol       arrange function */
	{ "  ",       tile },    /* first entry is default */
	{ "  ",      NULL },    /* no layout function means floating behavior */
	{ "  ",      monocle },
	{ "  ",      bstack },
	{ "  ",      bstackhoriz },
    { "  ",      clean },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]      = { "dmenu_run", "-h", barheight, "-m", dmenumon, "-fn", dmenufont, "-nb", darknormbg, "-nf", darknormfg, "-sb", darknormbg, "-sf", darkselfg, NULL };
static const char *termcmd[]       = { "st", NULL };
static const char *mutecmd[]       = { "amixer", "-q", "set", "Master", "toggle", NULL };
static const char *volupcmd[]      = { "amixer", "-q", "set", "Master", "5%+", NULL };
static const char *voldncmd[]      = { "amixer", "-q", "set", "Master", "5%-", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", "-f", "dina:pixelsize=11", NULL };
static const char *scrotcmd[]	   = { "scrot", "--exec", "mv -v $f ~/Pictures/Screenshots/", NULL };
static const char *shutdown[]      = { "turnoff.sh", NULL  };
static const char *lockcmd[]       = { "betterlockscreen", "-l", "dimblur", NULL  };
static const char *brightupcmd[]      = { "xbacklight", "-inc", "10", NULL};
static const char *brightdncmd[]      = { "xbacklight", "-dec", "10", NULL};

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_BackSpace,  togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
    { MODKEY|ShiftMask,             XK_j,      setcfact,       {.f = +0.030} },
    { MODKEY|ShiftMask,             XK_k,      setcfact,       {.f = -0.030} },
    { MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_u,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_o,      setlayout,      {.v = &layouts[4]} },
    { MODKEY,                       XK_c,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_t,      schemeToggle,   {0} },
	{ MODKEY|ShiftMask,             XK_z,      schemeCycle,    {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	/* custom */
    { False,                        XF86XK_AudioMute,          spawn,          {.v = mutecmd } },
    { False,                        XF86XK_AudioRaiseVolume,   spawn,          {.v = volupcmd } },
    { False,                        XF86XK_AudioLowerVolume,   spawn,          {.v = voldncmd } },
    { False,                        XF86XK_MonBrightnessUp,    spawn,          {.v = brightupcmd } },
    { False,                        XF86XK_MonBrightnessDown,  spawn,          {.v = brightdncmd } },
	{ False,                        XK_Print,                  spawn,          {.v = scrotcmd } },
	{ MODKEY|ShiftMask,             XK_F4,                     spawn,          {.v = shutdown } },
	{ MODKEY|ShiftMask,             XK_x,                      spawn,          {.v = lockcmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

